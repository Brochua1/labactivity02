package hellopackage;

import java.util.Random;
import java.util.Scanner;

import secondpackage.Utilities;

public class Greeter{
    public static void main(String[] args){
        Scanner kb = new Scanner(System.in);
        Random rng = new Random();

        System.out.println("Enter a number");

        int num = kb.nextInt();

        System.out.println("Here is double your number 23" + Utilities.doubleMe(num));
    }
}